\contentsline {chapter}{\numberline {第一章\hspace {.3em}}绪论}{1}{chapter.1}% 
\contentsline {section}{\numberline {1.1}研究背景和意义}{1}{section.1.1}% 
\contentsline {section}{\numberline {1.2}研究现状和进展}{2}{section.1.2}% 
\contentsline {subsection}{\numberline {1.2.1}视觉注意机制的研究现状}{2}{subsection.1.2.1}% 
\contentsline {subsubsection}{\numberline {（1）}自下而上视觉注意模型}{2}{subsubsection.1.2.1.1}% 
\contentsline {subsubsection}{\numberline {（2）}自上而下视觉注意模型}{2}{subsubsection.1.2.1.2}% 
\contentsline {subsection}{\numberline {1.2.2}目标检测的研究现状}{3}{subsection.1.2.2}% 
\contentsline {subsubsection}{\numberline {（1）}遥感图像目标检测}{3}{subsubsection.1.2.2.1}% 
\contentsline {subsubsection}{\numberline {（2）}静止图像目标检测}{3}{subsubsection.1.2.2.2}% 
\contentsline {section}{\numberline {1.3}本文主要研究内容及组织结构}{3}{section.1.3}% 
\contentsline {subsection}{\numberline {1.3.1}课题的研究内容}{3}{subsection.1.3.1}% 
\contentsline {subsection}{\numberline {1.3.2}本文的组织结构}{3}{subsection.1.3.2}% 
\contentsline {chapter}{\numberline {第二章\hspace {.3em}}基于视觉注意机制的显著图生成}{5}{chapter.2}% 
\contentsline {section}{\numberline {2.1}引言}{5}{section.2.1}% 
\contentsline {section}{\numberline {2.2}视觉注意处理机制}{5}{section.2.2}% 
\contentsline {subsection}{\numberline {2.2.1}视觉信息处理过程}{5}{subsection.2.2.1}% 
\contentsline {subsubsection}{\numberline {（1）}视觉感知}{5}{subsubsection.2.2.1.1}% 
\contentsline {subsubsection}{\numberline {（2）}视觉处理}{6}{subsubsection.2.2.1.2}% 
\contentsline {subsection}{\numberline {2.2.2}视觉注意计算模型}{6}{subsection.2.2.2}% 
\contentsline {section}{\numberline {2.3}提取底层特征}{7}{section.2.3}% 
\contentsline {subsection}{\numberline {2.3.1}亮度特征}{9}{subsection.2.3.1}% 
\contentsline {subsection}{\numberline {2.3.2}颜色特征}{9}{subsection.2.3.2}% 
\contentsline {subsection}{\numberline {2.3.3}方向特征}{10}{subsection.2.3.3}% 
\contentsline {subsection}{\numberline {2.3.4}区域对比度特征}{10}{subsection.2.3.4}% 
\contentsline {section}{\numberline {2.4}显著图构建与融合}{12}{section.2.4}% 
\contentsline {subsection}{\numberline {2.4.1}显著图构建}{12}{subsection.2.4.1}% 
\contentsline {subsection}{\numberline {2.4.2}显著图融合}{13}{subsection.2.4.2}% 
\contentsline {section}{\numberline {2.5}模型仿真与实验分析}{13}{section.2.5}% 
\contentsline {section}{\numberline {2.6}本章小结}{15}{section.2.6}% 
\contentsline {chapter}{\numberline {第三章\hspace {.3em}}基于AdaBoost算法的特征显著图权值优化}{16}{chapter.3}% 
\contentsline {section}{\numberline {3.1}引言}{16}{section.3.1}% 
\contentsline {section}{\numberline {3.2}Boosting算法}{16}{section.3.2}% 
\contentsline {section}{\numberline {3.3}AdaBoost算法的提出}{17}{section.3.3}% 
\contentsline {section}{\numberline {3.4}适用于多分类的Adaboost算法}{18}{section.3.4}% 
\contentsline {section}{\numberline {3.5}算法仿真与实验分析}{19}{section.3.5}% 
\contentsline {subsubsection}{\numberline {（1）}序列boat1}{20}{subsubsection.3.5.0.1}% 
\contentsline {subsubsection}{\numberline {（2）}序列car1}{20}{subsubsection.3.5.0.2}% 
\contentsline {subsubsection}{\numberline {（3）}序列bike1}{21}{subsubsection.3.5.0.3}% 
\contentsline {section}{\numberline {3.6}本章小结}{21}{section.3.6}% 
\contentsline {chapter}{\numberline {第四章\hspace {.3em}}基于视觉注意机制的多目标显著区域提取检测方法}{22}{chapter.4}% 
\contentsline {section}{\numberline {4.1}引言}{22}{section.4.1}% 
\contentsline {section}{\numberline {4.2}人眼关注点检测}{23}{section.4.2}% 
\contentsline {section}{\numberline {4.3}注意焦点的选择与转移}{24}{section.4.3}% 
\contentsline {section}{\numberline {4.4}区域分割及筛选}{25}{section.4.4}% 
\contentsline {subsection}{\numberline {4.4.1}区域分割}{25}{subsection.4.4.1}% 
\contentsline {subsection}{\numberline {4.4.2}区域筛选}{25}{subsection.4.4.2}% 
\contentsline {section}{\numberline {4.5}模型仿真与实验分析}{25}{section.4.5}% 
\contentsline {section}{\numberline {4.6}本章小结}{27}{section.4.6}% 
\contentsline {chapter}{\numberline {第五章\hspace {.3em}}总结与展望}{28}{chapter.5}% 
\contentsline {section}{\numberline {5.1}论文工作总结}{28}{section.5.1}% 
\contentsline {section}{\numberline {5.2}研究展望}{28}{section.5.2}% 
\contentsline {chapter}{致谢}{31}{appendix*.24}% 
\contentsfinish 
